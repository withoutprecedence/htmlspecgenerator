﻿using Xunit;

namespace SampleTestProject.Weapons.ShipMounted
{
    [Trait(Domains.Weapons, Modules.ShipMounted)]
    public class TurboLaserTests
    {
        [Fact]
        public void Holding_down_the_trigger_enables_autofire() {}

        [Fact]
        public void Pressing_trigger_releases_single_shot() {}
    }
}
