﻿using Xunit;

namespace SampleTestProject.Weapons.DomainServices.StationBatteries
{
    [Trait(Domains.Weapons, Modules.StationBatteries)]
    public class LaserTurretTests
    {
        [Fact]
        public void Firing_lasers_consume_power() {}

        [Fact]
        public void Selecting_target_initiates_tracking_procedure() {}

        [Fact]
        public void Tracking_target_moving_faster_than_tracking_speed_shows_warning() {}

        [Fact]
        public void Selecting_friendly_target_prompts_for_verification() {}
    }
}
