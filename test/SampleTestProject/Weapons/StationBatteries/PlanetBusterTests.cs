﻿using Xunit;

namespace SampleTestProject.Weapons.StationBatteries
{
    [Trait(Domains.Weapons, Modules.StationBatteries)]
    public class PlanetBusterTests
    {
        [Fact]
        public void Initiating_firing_sequence_calculates_firing_trajectory() {}

        [Fact]
        public void Initiating_firing_routes_power_to_the_main_weapon_systems() {}

        [Fact]
        public void After_weapon_is_fired_destruction_verification_routine_is_triggered() {}
    }
}
