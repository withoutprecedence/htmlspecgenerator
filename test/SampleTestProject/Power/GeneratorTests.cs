﻿using Xunit;

namespace SampleTestProject.Power
{
    public class GeneratorTests
    {
        [Fact]
        public void Overloading_a_generator_increases_power_output() {}

        [Fact]
        public void When_generator_explodes_chain_reaction_occurs() {}
    }
}
