﻿using Xunit;

namespace SampleTestProject.Spaceships.Fighters
{
    [Trait(Domains.Spaceships, Modules.Fighters)]
    public class DFighterTests
    {
        [Fact]
        public void You_cannot_assign_a_pilot_to_a_fighter_that_already_has_a_pilot() {}

        [Fact]
        public void Increasing_thrust_adjusts_fuel_consumption_accordingly() {}

        [Fact]
        public void When_fuel_levels_goes_under_ten_percent_the__low_fuel_indicator_blinks() {}
    }
}
