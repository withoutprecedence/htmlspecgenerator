﻿using Xunit;

namespace SampleTestProject.SpaceStations
{
    public class BluePrintTests
    {
        [Fact]
        public void When_analyzing_blue_prints_structural_weaknesses_are_identifies() {}

        [Fact]
        public void The_analyzing_person_must_have_an_engineering_degree() {}

        [Fact]
        public void Stealing_a_blueprint_removes_it_from_inventory() {}
    }
}
