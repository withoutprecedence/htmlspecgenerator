﻿using Xunit;

namespace SampleTestProject.SpaceStatiuons
{
    public class StarOfDeathTests
    {
        [Fact]
        public void Starting_construction_requires_schedule() {}

        [Fact]
        public void Completing_construction_triggers_imperial_visit() {}
    }
}
