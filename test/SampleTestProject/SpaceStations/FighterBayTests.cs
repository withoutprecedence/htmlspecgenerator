﻿using Xunit;

namespace SampleTestProject.SpaceStations
{
    public class FighterBayTests
    {
        [Fact]
        public void When_a_fighter_launches_the_bay_capacity_is_increased() {}

        [Fact]
        public void When_a_fighter_lands_the_bay_capacity_is_reduced() {}

        [Fact]
        public void A_fighter_requesting_landing_permission_is_denied_if_there_is_not_sufficient_capacity() {}
    }
}
