﻿using Xunit;

namespace SampleTestProject.Personnel.Soldiers
{
    [Trait(Domains.Personnel, Modules.Soldiers)]
    public class PilotTests
    {
        [Fact]
        public void A_pilot_must_have_pilot_certificate() {}

        [Fact]
        public void _It_cannot_be_expired() { }

        [Fact]
        public void Creating_a_pilot_without_a_spaceship_sets_his_status_as_reserve() {}
    }
}
