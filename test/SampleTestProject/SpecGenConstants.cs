﻿namespace SampleTestProject
{
    static class Domains
    {
        public const string Weapons = "Weapons";
        public const string Spaceships = "Spaceships";
        public const string Personnel = "Personnel";
    }

    static class Modules
    {
        public const string StationBatteries = "Station Batteries";
        public const string ShipMounted = "Ship Mounted";
        public const string Fighters = "Fighters";
        public const string Cruisers = "Cruisers";
        public const string Soldiers = "Soldiers";
        public const string Contractors = "Contractors";
        public const string Staff = "Staff";
    }
}
