﻿using SampleTestProject.Personnel.Soldiers;
using SpecGen.Spec;
using Xunit;

namespace SpecGen.Tests.Spec
{
    public class ProductSpecificationTests
    {
        [Fact]
        public void You_can_generate_a_production_specification_from_a_number_of_test_assemblies()
        {
            var testAssembly = typeof(PilotTests).Assembly;

            var result = ProductSpecification.Create("title", "version", new SpecScanner(), testAssembly);

            Assert.NotNull(result);
            Assert.Equal("title", result.Title);
            Assert.Equal("version", result.Version);
        }

        [Fact]
        public void Product_specifications_contain_the_specifications_from_the_assemblies()
        {
            var testAssembly = typeof(PilotTests).Assembly;

            var result = ProductSpecification.Create("title", "version", new SpecScanner(), testAssembly);

            Assert.NotEmpty(result.AssemblySpecifications);
        }

        [Fact]
        public void Product_specifications_metadata_is_extracted_from_the_assembly()
        {
            var testAssembly = typeof(PilotTests).Assembly;

            var result = ProductSpecification.Create("title", "version", new SpecScanner(), testAssembly);

            Assert.NotEmpty(result.AssemblySpecifications);
            Assert.Contains(result.AssemblySpecifications, g => g.Name == "Space Assets");
            Assert.Contains(result.AssemblySpecifications, g => g.Description == "Assets for a space game");
        }

        [Fact]
        public void Product_specifications_metadata_defaults_to_NA()
        {
            var testAssembly = typeof(PilotTests).Assembly;

            var result = ProductSpecification.Create("title", "version", new SpecScanner(), GetType().Assembly);

            Assert.NotEmpty(result.AssemblySpecifications);
            Assert.Contains(result.AssemblySpecifications, g => g.Description == "N/A");
        }
    }
}
