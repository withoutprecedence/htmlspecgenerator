﻿using SpecGen.Spec;
using System;
using Xunit;

namespace SpecGen.Tests.Spec
{
    public class DomainGroupTests
    {
        [Fact]
        public void Domain_group_names_get_spacing_added_before_capital_letters()
        {
            var result = new DomainGroup("SpaceStations", new Type[0]);

            Assert.Equal("Space Stations", result.Name);
        }
    }
}
