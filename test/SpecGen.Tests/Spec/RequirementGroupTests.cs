﻿using SampleTestProject.Personnel.Soldiers;
using SampleTestProject.Power;
using SpecGen.Spec;
using Xunit;

namespace SpecGen.Tests.Spec
{
    public class RequirementGroupTests
    {
        [Fact]
        public void Requirements_are_named_after_the_tests()
        {
            var sut = new RequirementGroup(typeof(GeneratorTests));

            Assert.Equal("Overloading a generator increases power output", sut.Requirements[0].Name);
            Assert.Equal("When generator explodes chain reaction occurs", sut.Requirements[1].Name);
        }

        [Fact]
        public void Tests_prefixed_with_an_underscore_are_treated_as_limitations()
        {
            var sut = new RequirementGroup(typeof(PilotTests));

            Assert.False(sut.Requirements[0].IsLimitation);
            Assert.True(sut.Requirements[1].IsLimitation);
            Assert.False(sut.Requirements[2].IsLimitation);
        }
    }
}
