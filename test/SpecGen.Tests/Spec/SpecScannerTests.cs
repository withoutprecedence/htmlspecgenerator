﻿using SampleTestProject.Personnel.Contractors;
using SpecGen.Spec;
using System.Linq;
using System.Reflection;
using Xunit;

namespace SpecGen.Tests.Spec
{
    public class SpecScannerTests
    {
        private static readonly Assembly _specAssembly = typeof(EngineerTests).Assembly;

        [Fact]
        public void You_can_scan_an_assembly_for_a_specification()
        {
            var sut = new SpecScanner();

            var result = sut.ScanAssembly(_specAssembly);

            Assert.NotNull(result);
        }

        [Fact]
        public void Tests_are_placed_into_domain_groups_based_on_their_namespace()
        {
            var sut = new SpecScanner();

            var result = sut.ScanAssembly(_specAssembly);

            Assert.Contains(result.DomainGroups, g => g.Name == "Power");
        }

        [Fact]
        public void You_can_group_requirement_groups_higher_up_in_the_folder_hierarchy()
        {
            var sut = new SpecScanner(skipNamespaceLevels: 1);

            var result = sut.ScanAssembly(_specAssembly);

            Assert.Contains(result.DomainGroups, g => g.Name == "Personnel" && g.RequirementGroups.Count == 6);
        }

        [Fact]
        public void By_default_a_domain_group_has_one_requirements_group_per_test_type()
        {
            var sut = new SpecScanner();

            var result = sut.ScanAssembly(_specAssembly);

            var domainGroup = result.DomainGroups.First(g => g.Name == "Soldiers");
            Assert.Equal(2, domainGroup.RequirementGroups.Count);
        }

        [Fact]
        public void Requirements_groups_are_named_after_the_test_types()
        {
            var sut = new SpecScanner();

            var result = sut.ScanAssembly(_specAssembly);

            var domainGroup = result.DomainGroups.First(g => g.Name == "Soldiers");
            Assert.Equal("Pilot", domainGroup.RequirementGroups[0].Name);
            Assert.Equal("Trooper", domainGroup.RequirementGroups[1].Name);
        }

        [Fact]
        public void The_default_requirements_group_has_one_requirement_per_method()
        {
            var sut = new SpecScanner();

            var result = sut.ScanAssembly(_specAssembly);

            var domainGroup = result.DomainGroups.First(g => g.Name == "Power");
            var requirementsGroup = domainGroup.RequirementGroups[0];
            Assert.Equal(2, requirementsGroup.Requirements.Count);
        }

        [Fact]
        public void You_can_filter_out_domain_groups()
        {
            var sut = new SpecScanner(exclude: "Power");

            var result = sut.ScanAssembly(_specAssembly);

            Assert.DoesNotContain(result.DomainGroups, g => g.Name == "Power");
        }

        [Fact]
        public void Excluding_is_case_insensitive()
        {
            var sut = new SpecScanner(exclude: "PoWeR");

            var result = sut.ScanAssembly(_specAssembly);

            Assert.DoesNotContain(result.DomainGroups, g => g.Name == "Power");
        }

        [Fact]
        public void You_can_include_only_specific_domain_groups()
        {
            var sut = new SpecScanner(include: "Power");

            var result = sut.ScanAssembly(_specAssembly);

            Assert.NotEmpty(result.DomainGroups);
            Assert.All(result.DomainGroups, g => Assert.Equal("Power", g.Name));
        }

        [Fact]
        public void Including_is_case_insensitive()
        {
            var sut = new SpecScanner(include: "PoWeR");

            var result = sut.ScanAssembly(_specAssembly);

            Assert.NotEmpty(result.DomainGroups);
            Assert.All(result.DomainGroups, g => Assert.Equal("Power", g.Name));
        }
    }
}
