﻿using SampleTestProject.Personnel.Contractors;
using SpecGen.Spec;
using System.Reflection;
using Xunit;

namespace SpecGen.Tests.Spec
{
    public class SpecificationFormatterTests
    {
        private static readonly Assembly _specAssembly = typeof(EngineerTests).Assembly;

        [Fact]
        public void You_can_format_a_product_specification_as_HTML()
        {
            var sut = new SpecificationFormatter();
            var spec = ProductSpecification.Create("title", "version", new SpecScanner(), _specAssembly);

            var result = sut.FormatHtml(spec);

            Assert.NotNull(result);
        }

        [Fact]
        public void The_HTML_title_is_set_from_the_specification()
        {
            var sut = new SpecificationFormatter();
            var spec = ProductSpecification.Create("title", "version", new SpecScanner(), _specAssembly);

            var result = sut.FormatHtml(spec);

            Assert.Contains("<title>title</title>", result);
        }
    }
}
