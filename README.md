# HtmlSpecGenerator

A .NET Core based command line utility for generating HTML documentation from unit tests.

## Test

To test, run with the following argument

```
-p ../../../../../test/SampleTestProject/bin/Debug/netcoreapp2.2/SampleTestProject.dll -t "Space Stuff" -v 1.0.2
```