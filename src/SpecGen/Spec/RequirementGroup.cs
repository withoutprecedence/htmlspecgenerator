﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace SpecGen.Spec
{
    /// <summary>
    /// A grouping of related requirements.
    /// </summary>
    class RequirementGroup
    {
        public string Name { get; }
        public IReadOnlyList<Requirement> Requirements { get; } = new List<Requirement>();

        public RequirementGroup(Type type)
        {
            string CleanName() =>
                TrimEnd(TrimEnd(type.Name, "Test"), "Tests");

            List<Requirement> GetRequirements() =>
                type
                .GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly)
                .Where(m => m.CustomAttributes.Any(a => a.AttributeType.Name == "FactAttribute" || a.AttributeType.Name == "TheoryAttribute"))
                .Select(r => new Requirement(r.Name))
                .ToList();

            Name = CleanName();
            Requirements = GetRequirements();
        }

        private static string TrimEnd(string source, string value)
        {
            if (!source.EndsWith(value, StringComparison.InvariantCultureIgnoreCase))
                return source;

            return source.Remove(source.LastIndexOf(value, StringComparison.InvariantCultureIgnoreCase));
        }
    }
}
