﻿namespace SpecGen.Spec
{
    /// <summary>
    /// A business requirement.
    /// </summary>
    class Requirement
    {
        public string Name { get; }
        public bool IsLimitation { get; }

        public Requirement(string name)
        {
            Name = name.Replace('_', ' ').Trim();
            IsLimitation = name.StartsWith('_');
        }
    }
}
