﻿using System;
using System.Linq;
using System.Reflection;

namespace SpecGen.Spec
{
    class SpecScanner
    {
        private readonly int _skipNamespaceLevels;
        private readonly string _exclude;
        private readonly string _include;

        public SpecScanner(int skipNamespaceLevels = 0, string exclude = null, string include = null)
        {
            _skipNamespaceLevels = skipNamespaceLevels;
            _exclude = exclude;
            _include = include;
        }

        public AssemblySpecification ScanAssembly(Assembly specAssembly)
        {
            bool HasValidNamespace(Type t) =>
                t.Namespace != null && _skipNamespaceLevels <= t.Namespace.Count(c => c == '.');

            bool IsNotExcluded(Type t) =>
                _exclude == null || !t.Namespace.Contains(_exclude, StringComparison.InvariantCultureIgnoreCase);

            bool IsIncluded(Type t) =>
                _include == null || t.Namespace.Contains(_include, StringComparison.InvariantCultureIgnoreCase);

            string GetDomainName(Type type) =>
                type.Namespace.Split('.').Reverse().Skip(_skipNamespaceLevels).FirstOrDefault();

            var domainNames = specAssembly
                .GetTypes()
                .Where(HasValidNamespace)
                .Where(IsNotExcluded)
                .Where(IsIncluded)
                .GroupBy(GetDomainName)
                .Distinct();

            var name = specAssembly.GetCustomAttribute<AssemblyProductAttribute>()?.Product ?? "N/A";
            var description = specAssembly.GetCustomAttribute<AssemblyDescriptionAttribute>()?.Description ?? "N/A";

            return new AssemblySpecification(name, description, domainNames.Select(domainTypes => new DomainGroup(domainTypes.Key, domainTypes)));
        }
    }
}
