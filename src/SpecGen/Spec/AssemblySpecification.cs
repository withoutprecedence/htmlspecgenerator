﻿using System.Collections.Generic;
using System.Linq;

namespace SpecGen.Spec
{
    class AssemblySpecification
    {
        public AssemblySpecification(string name, string description, IEnumerable<DomainGroup> domainGroups)
        {
            Name = name;
            Description = description;
            DomainGroups = domainGroups.ToList();
        }

        public string Name { get; }
        public string Description { get; }
        public IReadOnlyList<DomainGroup> DomainGroups { get; }
    }
}
