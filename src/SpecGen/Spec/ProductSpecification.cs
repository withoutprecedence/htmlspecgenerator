﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace SpecGen.Spec
{
    class ProductSpecification
    {
        private ProductSpecification(string title, string version, IEnumerable<AssemblySpecification> assemblySpecifications)
        {
            Title = title;
            Version = version;
            AssemblySpecifications = assemblySpecifications.ToList();
        }

        public string Title { get; }
        public string Version { get; }
        public IReadOnlyList<AssemblySpecification> AssemblySpecifications { get; }

        public static ProductSpecification Create(string title, string version, SpecScanner scanner, params Assembly[] testAssemblies)
        {
            var assemblySpecifications = testAssemblies.Select(a => scanner.ScanAssembly(a));
            return new ProductSpecification(title, version, assemblySpecifications);
        }
    }
}
