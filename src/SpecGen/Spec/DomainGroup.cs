﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace SpecGen.Spec
{
    /// <summary>
    /// A grouping within a domain, for which business requirements apply.
    /// </summary>
    class DomainGroup
    {
        public string Name { get; }
        public IReadOnlyList<RequirementGroup> RequirementGroups { get; }

        public DomainGroup(string name, IEnumerable<Type> requirementTypes)
        {
            string FormatName() =>
                Regex.Replace(name, "([a-z])([A-Z])", "$1 $2");

            List<RequirementGroup> GetRequirementGroups() =>
                requirementTypes.Select(t => new RequirementGroup(t)).ToList();

            Name = FormatName();
            RequirementGroups = GetRequirementGroups();
        }
    }
}
