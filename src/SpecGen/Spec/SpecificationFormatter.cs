﻿using System;
using System.IO;
using System.Linq;

namespace SpecGen.Spec
{
    class SpecificationFormatter
    {
        public string FormatHtml(ProductSpecification productSpecification)
        {
            var body = FormatDomains(productSpecification);
            return FormatDocument(productSpecification, body);
        }

        private static string FormatDocument(ProductSpecification productSpecification, string body) =>
            $@"<html>
                <head>
                    <meta charset=""utf-8"">
                    <title>{productSpecification.Title}</title>
                    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
                    <script src=""https://code.jquery.com/jquery-3.3.1.min.js"" integrity=""sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="" crossorigin=""anonymous""></script>
                </head>
            <body>
                <style>{ReadCssFile()}</style>
            {body}
            <script>
                $(function(){{
                    $('.requirement-group-header').click(function(){{
                        $(this).next().toggleClass('hidden');
					}});
					
					$search = $('#search');

					$search.keyup((e) => {{
						var seachValue = $search.val().toLowerCase();
						if (seachValue) {{
							$('body').addClass('searching');
							$('.domain').each((idx, e) => {{
								if (e.textContent.toLowerCase().includes(seachValue)) {{
									$(e).removeClass('filtered');
								}} else {{
									$(e).addClass('filtered');
								}}
							}});
							$('li').each((idx, e) => {{
								if (e.textContent.toLowerCase().includes(seachValue)) {{
									$(e).removeClass('filtered');
								}} else {{
									$(e).addClass('filtered');
								}}
							}});
						}} else {{
							$('body').removeClass('searching');
						}}
					}});
                }});
            </script>
            </body>
        </html>";



        static string ReadCssFile()
        {
            var assembly = typeof(Program).Assembly;
            using (var input = assembly.GetManifestResourceStream(assembly.GetName().Name + ".Files.style.css"))
            using (var reader = new StreamReader(input))
            {
                return reader.ReadToEnd();
            }
        }

        private int _nestingLevel;

        private string FormatDomains(ProductSpecification productSpecification)
        {
            _nestingLevel = 1;

            return Div("product", "<h1>" + productSpecification.Title + "</h1>\r\n" +
                GetNesting() + "\t<h3>" + productSpecification.Version + "</h3>\r\n" +
                GetNesting() + "\t<h4>" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + "</h4>\r\n" +
                GetNesting() + "\t<input id=\"search\" type=\"text\" />" +
                string.Join("\r\n", productSpecification.AssemblySpecifications.OrderBy(a => a.Name).Select(FormatDomainsInAssemblySpecification)));
        }

        private string FormatDomainsInAssemblySpecification(AssemblySpecification specification)
        {
            if (specification.DomainGroups.Count == 0)
                return string.Empty;

            _nestingLevel++;
            return Div("assembly", "<h2>" + specification.Name + "</h2>\r\n" +
                GetNesting() + "\t<h4>" + specification.Description + "</h4>\r\n" +
                string.Join("\r\n", specification.DomainGroups.OrderBy(d => d.Name).Select(FormatDomain)));
        }

        private string FormatDomain(DomainGroup domain)
        {
            _nestingLevel++;
            return Div("domain", "<h3>" + domain.Name + "</h3>\r\n" +
                string.Join("\r\n", domain.RequirementGroups.Where(r => r.Requirements.Any()).OrderBy(r => r.Name).Select(FormatGroup)));// + "\r\n" +
        }

        private string FormatGroup(RequirementGroup group)
        {
            _nestingLevel++;
            return GetNesting() + "<h5 class='requirement-group-header'>" + group.Name + " (" + group.Requirements.Count + ")</h5>\r\n" +
                Ul(string.Join("\r\n", group.Requirements.Select(FormatRequirement)), "hidden");
        }

        private string FormatRequirement(Requirement requirement)
        {
            var @class = requirement.IsLimitation ? "limitation" : "requirement";
            return GetNesting() + $"\t<li class='{@class}'>" + requirement.Name + "</li>";
        }

        private string Div(string @class, string content)
        {
            return string.Format(
@"{2}<div class=""{0}"">
    {2}{1}
{2}</div>", @class, content, new string(Enumerable.Repeat('\t', _nestingLevel--).ToArray()));
        }

        private string Ul(string content, string @class = "")
        {
            return string.Format(
@"{1}<ul class=""{2}"">
{0}
{1}</ul>", content, new string(Enumerable.Repeat('\t', _nestingLevel--).ToArray()), @class);
        }

        private string GetNesting()
        {
            return new string(Enumerable.Repeat('\t', _nestingLevel).ToArray());
        }
    }
}
