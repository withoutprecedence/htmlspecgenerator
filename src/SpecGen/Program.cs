﻿using CommandLine;
using SpecGen.Spec;
using System;
using System.IO;

namespace SpecGen
{
    static class Program
    {
        static void Main(string[] args)
        {
            Parser.Default.ParseArguments<Options>(args).WithParsed(o =>
            {
                if (!File.Exists(o.Path))
                {
                    Console.WriteLine("Path does not point to a file: " + Path.GetFullPath(o.Path));
                    return;
                }

                Console.WriteLine("Generating spec");
                var html = GenerateHtml(o);
                SaveHtml(html);

            });

            Console.WriteLine("Done!");
        }

        private static string GenerateHtml(Options o)
        {
            using (var assemblyResolver = new AssemblyResolver(Path.GetFullPath(o.Path)))
            {
                var spec = ProductSpecification.Create(o.Title, o.Version, new SpecScanner(skipNamespaceLevels: 1, o.Exclude, o.Include), assemblyResolver.Assembly);
                return new SpecificationFormatter().FormatHtml(spec);
            }
        }

        static void SaveHtml(string html)
        {
            var htmlFile = new FileInfo("specification.html");
            if (!htmlFile.Exists)
                htmlFile.Create().Dispose();

            using (var fw = new StreamWriter(htmlFile.Open(FileMode.Truncate, FileAccess.Write)))
            {
                fw.Write(html);
            }
        }
    }
}
