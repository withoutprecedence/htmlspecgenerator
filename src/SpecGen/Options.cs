﻿using CommandLine;

namespace SpecGen
{
    class Options
    {
        [Option('p', "path", Required = true, HelpText = "The path to the test binary to generate a spec for")]
        public string Path { get; set; }

        [Option('t', "title", Required = true, HelpText = "The title of the specification")]
        public string Title { get; set; }

        [Option('v', "version", Required = true, HelpText = "The version of the specification")]
        public string Version { get; set; }

        [Option('i', "include", Required = false, HelpText = "A filter to include tests by namespace. Only tests with namespaces containing the filter will be included.")]
        public string Include { get; set; }

        [Option('e', "exclude", Required = false, HelpText = "A filter to exclude tests by namespace. Tests with namespaces containing the filter will be excluded.")]
        public string Exclude { get; set; }
    }
}
